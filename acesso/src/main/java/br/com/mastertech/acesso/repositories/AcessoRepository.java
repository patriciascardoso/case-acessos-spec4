package br.com.mastertech.acesso.repositories;

import br.com.mastertech.acesso.models.Acesso;
import org.checkerframework.checker.units.qual.A;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository <Acesso, Integer> {

    Optional<Acesso> findByClienteIdAndPortaId (int clienteId, int portaId);

    void deleteAllByClienteIdAndPortaId(int clienteId, int portaId);
}
