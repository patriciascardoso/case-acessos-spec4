package br.com.mastertech.acesso.services;

import br.com.mastertech.acesso.DTOs.AcessoDTO;
import br.com.mastertech.acesso.client.Cliente;
import br.com.mastertech.acesso.client.ClienteClient;
import br.com.mastertech.acesso.client.Porta;
import br.com.mastertech.acesso.client.PortaClient;
import br.com.mastertech.acesso.converters.AcessoConverter;
import br.com.mastertech.acesso.exceptions.AcessoNotFoundException;
import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private AcessoConverter acessoConverter;

    @Autowired
    private AcessoProducer acessoProducer;



    public Acesso cadastrarAcesso (Acesso acesso){
        Acesso acessoCadastro = new Acesso();

        Porta porta = portaClient.getPortaById(acesso.getPortaId());
        Cliente cliente = clienteClient.getClienteById(acesso.getClienteId());

        acessoCadastro.setPortaId(porta.getId());
        acessoCadastro.setClienteId(cliente.getId());

        acessoRepository.save(acessoCadastro);


        return acessoCadastro;
    }

    public Acesso consultarAcesso(  int clienteId , int portaId){

        Porta porta = portaClient.getPortaById(portaId);
        Cliente cliente = clienteClient.getClienteById(clienteId);

        Optional<Acesso> acessoOptional = acessoRepository.findByClienteIdAndPortaId(cliente.getId(), porta.getId());

        if(!acessoOptional.isPresent()){
            Acesso acesso1 = new Acesso();
            acesso1.setClienteId(clienteId);
            acesso1.setPortaId(portaId);
            acessoProducer.enviarAoKafka(acessoConverter.acessoToAcessoLog(acesso1,false));
            throw new AcessoNotFoundException();
        }

        acessoProducer.enviarAoKafka(acessoConverter.acessoToAcessoLog(acessoOptional.get(), true));
        return acessoOptional.get();
    }


    @Transactional
    public void deletarAcesso (int clienteId, int portaId){
        Porta porta = portaClient.getPortaById(portaId);
        Cliente cliente = clienteClient.getClienteById(clienteId);

        acessoRepository.deleteAllByClienteIdAndPortaId(clienteId, portaId);
    }
}
