package br.com.mastertech.acesso.client;

import br.com.mastertech.acesso.exceptions.ClienteNotFoundException;
import br.com.mastertech.acesso.exceptions.PortaNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder{

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404){
            return new PortaNotFoundException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
