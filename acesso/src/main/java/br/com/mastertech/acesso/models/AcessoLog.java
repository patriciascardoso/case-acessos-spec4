package br.com.mastertech.acesso.models;

public class AcessoLog {

    private int portaId;

    private int clienteId;

    private boolean acessoPermitido;


    public AcessoLog() {
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAcessoPermitido() {
        return acessoPermitido;
    }

    public void setAcessoPermitido(boolean acessoPermitido) {
        this.acessoPermitido = acessoPermitido;
    }
}
