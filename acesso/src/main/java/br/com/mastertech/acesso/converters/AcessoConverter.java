package br.com.mastertech.acesso.converters;

import br.com.mastertech.acesso.DTOs.AcessoDTO;
import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.models.AcessoLog;
import org.springframework.stereotype.Component;

@Component
public class AcessoConverter {

    public AcessoDTO converterAcessoToAcessoDTO(Acesso acesso){
        AcessoDTO acessoDTO = new AcessoDTO();
        acessoDTO.setCliente_id(acesso.getClienteId());
        acessoDTO.setPorta_id(acesso.getPortaId());

        return acessoDTO;
    }

    public Acesso converterAcessoDTOtoAcesso (AcessoDTO acessoDTO){
        Acesso acesso = new Acesso();
        acesso.setClienteId(acessoDTO.getCliente_id());
        acesso.setPortaId(acessoDTO.getPorta_id());

        return acesso;
    }

    public AcessoLog acessoToAcessoLog(Acesso acesso, boolean acessoPermitido){
        AcessoLog acessoLog = new AcessoLog();
        acessoLog.setClienteId(acesso.getClienteId());
        acessoLog.setPortaId(acesso.getPortaId());
        acessoLog.setAcessoPermitido(acessoPermitido);
        return acessoLog;
    }

}
