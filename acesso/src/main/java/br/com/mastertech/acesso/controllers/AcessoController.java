package br.com.mastertech.acesso.controllers;

import br.com.mastertech.acesso.DTOs.AcessoDTO;
import br.com.mastertech.acesso.converters.AcessoConverter;
import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping ("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoConverter acessoConverter;

    @PostMapping
    @ResponseStatus (HttpStatus.CREATED)
    public AcessoDTO cadastrarAcesso (@RequestBody AcessoDTO acessoDTO){
        Acesso acesso =  acessoConverter.converterAcessoDTOtoAcesso(acessoDTO);

        Acesso acessoCadastrado = acessoService.cadastrarAcesso(acesso);

     return acessoConverter.converterAcessoToAcessoDTO(acessoCadastrado);

    }

    @GetMapping ("/{cliente_id}/{porta_id}")
    public AcessoDTO consultarAcesso (@PathVariable (name = "cliente_id") int cliente_id,
                                   @PathVariable (name = "porta_id") int porta_id){

        Acesso acesso = acessoService.consultarAcesso(cliente_id, porta_id);

        return acessoConverter.converterAcessoToAcessoDTO(acesso);
    }

    @DeleteMapping ("/{cliente_id}/{porta_id}")
    @ResponseStatus (HttpStatus.NO_CONTENT)
    public void deletarAcesso (@PathVariable (name = "cliente_id") int cliente_id,
                                      @PathVariable (name = "porta_id") int porta_id){

        acessoService.deletarAcesso(cliente_id, porta_id);
    }
}
