package br.com.mastertech.cliente.services;

import br.com.mastertech.cliente.exceptions.ClienteNotFoundException;
import br.com.mastertech.cliente.models.Cliente;
import br.com.mastertech.cliente.repositories.ClienteRepository;
import org.hibernate.procedure.spi.ParameterRegistrationImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente (Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscarCliente (int id){
        Optional <Cliente> clienteOptional = clienteRepository.findById(id);

        if (!clienteOptional.isPresent()){
            throw new ClienteNotFoundException();
        }

        return clienteOptional.get();
    }
}
