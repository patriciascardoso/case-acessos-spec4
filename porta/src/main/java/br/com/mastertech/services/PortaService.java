package br.com.mastertech.services;

import br.com.mastertech.exceptions.PortaNotFoundException;
import br.com.mastertech.models.Porta;
import br.com.mastertech.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta cadastrarPorta (Porta porta){
        return portaRepository.save(porta);
    }

    public Porta consultaPorta (int id){
        Optional <Porta> portaOptional = portaRepository.findById(id);

        if(!portaOptional.isPresent()){
            throw new PortaNotFoundException();
        }
        return portaOptional.get();
    }

}
