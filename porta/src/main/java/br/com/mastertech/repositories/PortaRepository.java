package br.com.mastertech.repositories;

import br.com.mastertech.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository <Porta, Integer> {
}
