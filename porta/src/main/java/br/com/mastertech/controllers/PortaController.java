package br.com.mastertech.controllers;

import br.com.mastertech.models.Porta;
import br.com.mastertech.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping ("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus (HttpStatus.CREATED)
    public Porta cadastrarPorta (@RequestBody Porta porta) {
        return portaService.cadastrarPorta(porta);
    }

    @GetMapping("/{id}")
    public Porta buscarPorta (@PathVariable (name = "id") int id) {
        return portaService.consultaPorta(id);
    }
}
