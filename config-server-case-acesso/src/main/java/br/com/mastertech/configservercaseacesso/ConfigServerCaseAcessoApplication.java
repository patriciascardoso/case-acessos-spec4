package br.com.mastertech.configservercaseacesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigServerCaseAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigServerCaseAcessoApplication.class, args);
	}

}
